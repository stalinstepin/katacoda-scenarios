In this scenario you will understand on how to create a YAML file using YAML syntax.

## Task

Edit the **food.yml** file under /etc/ansible and add update the food.yml file to add a Vegetable - Carrot entry. Refer to the examples below:

**Fruit: Apple**

**Drink: Water**

**Dessert: Cake**


Execute the **command** provided below by either typing it on the terminal or by clicking on the command itself.


`vim /etc/ansible/food.yml`{{execute}}

`ansible --version`{{execute}}
